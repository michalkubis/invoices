<?php

namespace Invoices;

use DateTime;


/**
 * DataImpl - part of Eciovni plugin for Nette Framework.
 *
 * @copyright  Copyright (c) 2009 Ondřej Brejla
 * @license    New BSD License
 * @link       http://github.com/OndrejBrejla/Eciovni
 */
class DataImpl implements IData
{

	/** @var string */
	private $title;

	/** @var string */
	private $caption;

	/** @var string */
	private $signatureText;

	/** @var string */
	private $signatureImgSrc;

	/** @var string */
	private $id;

	/** @var IParticipant */
	private $supplier;

	/** @var IParticipant */
	private $customer;

	/** @var int */
	private $variableSymbol = 0;

	/** @var int */
	private $constantSymbol = 0;

	/** @var int */
	private $specificSymbol = 0;

	/** @var \DateTime */
	private $expirationDate;

	/** @var \DateTime */
	private $dateOfIssuance;

	/** @var \DateTime */
	private $dateOfVatRevenueRecognition;

	/** @var IItem[] */
	private $items = array();

	/** @var string */
	private $payMethod;

	/** @var string */
	private $currency;

	public function __construct(DataBuilder $dataBuilder)
	{
		$this->title = $dataBuilder->getTitle();
		$this->caption = $dataBuilder->getCaption();
		$this->signatureText = $dataBuilder->getSignatureText();
		$this->id = $dataBuilder->getId();
		$this->supplier = $dataBuilder->getSupplier();
		$this->customer = $dataBuilder->getCustomer();
		$this->variableSymbol = $dataBuilder->getVariableSymbol();
		$this->constantSymbol = $dataBuilder->getConstantSymbol();
		$this->specificSymbol = $dataBuilder->getSpecificSymbol();
		$this->expirationDate = $dataBuilder->getExpirationDate();
		$this->dateOfIssuance = $dataBuilder->getDateOfIssuance();
		$this->items = $dataBuilder->getItems();
		$this->payMethod = $dataBuilder->getPayMethod();
		$this->currency = $dataBuilder->getCurrency();

		if($dataBuilder->isDateOfVatRevenueRecognition())
		{
			$this->dateOfVatRevenueRecognition = $dataBuilder->getDateOfVatRevenueRecognition();
		}
	}

	/**
	 * Returns the invoice title.
	 *
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getCaption(): string
	{
		return $this->caption;
	}

	/**
	 * @return string
	 */
	public function getSignatureText(): string
	{
		return $this->signatureText;
	}

	/**
	 * Signature image
	 *
	 * @return string
	 */
	public function getSignatureImgSrc(): string
	{
		return $this->signatureImgSrc;
	}


	/**
	 * Returns the invoice id.
	 *
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * Returns the invoice supplier.
	 *
	 * @return IParticipant
	 */
	public function getSupplier(): IParticipant
	{
		return $this->supplier;
	}

	/**
	 * Returns the invoice customer.
	 *
	 * @return IParticipant
	 */
	public function getCustomer(): IParticipant
	{
		return $this->customer;
	}

	/**
	 * Returns the variable symbol.
	 *
	 * @return int
	 */
	public function getVariableSymbol(): int
	{
		return $this->variableSymbol;
	}

	/**
	 * Returns the constant symbol.
	 *
	 * @return int
	 */
	public function getConstantSymbol(): int
	{
		return $this->constantSymbol;
	}

	/**
	 * Returns the specific symbol.
	 *
	 * @return int
	 */
	public function getSpecificSymbol(): int
	{
		return $this->specificSymbol;
	}

	/**
	 * Returns the expiration date in defined format.
	 *
	 * @param string $format
	 * @return string
	 */
	public function getExpirationDate(string $format = 'd.m.Y'): string
	{
		return $this->expirationDate->format($format);
	}

	/**
	 * Returns the date of issuance in defined format.
	 *
	 * @param string $format
	 * @return string
	 */
	public function getDateOfIssuance(string $format = 'd.m.Y'): string
	{
		return $this->dateOfIssuance->format($format);
	}

	/**
	 * Returns the date of VAT revenue recognition in defined format.
	 *
	 * @param string $format
	 * @return string
	 */
	public function getDateOfVatRevenueRecognition(string $format = 'd.m.Y'): string
	{
		return $this->dateOfVatRevenueRecognition === NULL ? '' : $this->dateOfVatRevenueRecognition->format($format);
	}

	/**
	 * Returns the array of items.
	 *
	 * @return IItem[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

	/**
	 * Returns pay method of invoice
	 *
	 * @return string
	 */
	public function getPayMethod(): string
	{
		return $this->payMethod;
	}

	/**
	 * Returns currency of invoice
	 *
	 * @return string
	 */
	public function getCurrency(): string
	{
		return $this->currency;
	}
}
