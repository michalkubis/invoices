# Invoices
Component for generating invoices using Nette Framework and mPDF library.

For of fork of original library. I create this fork to test if it works with PHP 7.2 and mPDF 7.1.

# Install (with composer):

Edit composer.json.

```
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/michalkubis/invoices"
        }
        
    ...
    
    "require": {
        ...
        "mpdf/mpdf": "^6.1",
        "michalkubis/invoices": "v1.0"
```

For install all dopendecies just run
```
composer update
```

# Usage

Unfortunately, this is not completed yet. Once it is going to be complete, all necessary informations will be 
provided. 