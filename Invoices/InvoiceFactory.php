<?php declare(strict_types = 1);

namespace Invoices;

use Nette\Application\LinkGenerator;
use Nette\Application\UI\ITemplateFactory;

/**
 * @author Pavel Jurásek
 */
class InvoiceFactory
{

	/** @var ITemplateFactory */
	private $templateFactory;

	/** @var LinkGenerator */
	private $linkGenerator;

	public function __construct(ITemplateFactory $templateFactory, LinkGenerator $linkGenerator)
	{
		$this->templateFactory = $templateFactory;
		$this->linkGenerator = $linkGenerator;
	}

	public function create(string $invoiceLanguage, IData $data): Invoice
	{
		return new Invoice($invoiceLanguage, $data, $this->templateFactory, $this->linkGenerator);
	}

}
