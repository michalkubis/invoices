<?php

namespace Invoices;

/**
 * Item - part of Eciovni plugin for Nette Framework.
 *
 * @copyright  Copyright (c) 2009 Ondřej Brejla
 * @license    New BSD License
 * @link       http://github.com/OndrejBrejla/Eciovni
 */
interface IItem
{

	/**
	 * Returns the description of the item.
	 *
	 * @return string
	 */
	public function getDescription(): string;

	/**
	 * Returns the tax of the item.
	 *
	 * @return ITax
	 */
	public function getTax(): ITax;

	/**
	 * Returns the value of one unit of the item.
	 *
	 * @return double
	 */
	public function getUnitValue(): float;

	/**
	 * Returns the number of item units.
	 *
	 * @return int
	 */
	public function getUnits(): int;

	/**
	 * Returns the value of taxes for all units.
	 *
	 * @return double
	 */
	public function countTaxValue(): float;

	/**
	 * Returns the value of unit without tax.
	 *
	 * @return double
	 */
	public function countUntaxedUnitValue(): float;

	/**
	 * Returns the final value of all taxed units.
	 *
	 * @return double
	 */
	public function countFinalValue(): float ;

}
