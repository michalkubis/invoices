<?php

namespace Invoices;

/**
 * Data - part of Eciovni plugin for Nette Framework.
 *
 * @copyright  Copyright (c) 2009 Ondřej Brejla
 * @license    New BSD License
 * @link       http://github.com/OndrejBrejla/Eciovni
 */
interface IData
{

	/**
	 * Returns the invoice title.
	 *
	 * @return string
	 */
	public function getTitle(): string;

	/**
	 * @return string
	 */
	public function getCaption(): string;

	/**
	 * Signature text
	 *
	 * @return string
	 */
	public function getSignatureText(): string;

	/**
	 * Signature image
	 *
	 * @return string
	 */
	public function getSignatureImgSrc(): string;

	/**
	 * Returns the invoice id.
	 *
	 * @return string
	 */
	public function getId(): string;

	/**
	 * Returns the invoice supplier.
	 *
	 * @return IParticipant
	 */
	public function getSupplier(): IParticipant;

	/**
	 * Returns the invoice customer.
	 *
	 * @return IParticipant
	 */
	public function getCustomer(): IParticipant;

	/**
	 * Returns the variable symbol.
	 *
	 * @return int
	 */
	public function getVariableSymbol(): int;

	/**
	 * Returns the constant symbol.
	 *
	 * @return int
	 */
	public function getConstantSymbol(): int;

	/**
	 * Returns the specific symbol.
	 *
	 * @return int
	 */
	public function getSpecificSymbol(): int;

	/**
	 * Returns the expiration date in defined format.
	 *
	 * @param string $format
	 * @return string
	 */
	public function getExpirationDate(string $format = 'd.m.Y'): string;

	/**
	 * Returns the date of issuance in defined format.
	 *
	 * @param string $format
	 * @return string
	 */
	public function getDateOfIssuance(string $format = 'd.m.Y'): string;

	/**
	 * Returns the date of VAT revenue recognition in defined format.
	 *
	 * @param string $format
	 * @return string
	 */
	public function getDateOfVatRevenueRecognition(string $format = 'd.m.Y'): string;

	/**
	 * Returns the array of items.
	 *
	 * @return IItem[]
	 */
	public function getItems(): array;

	/**
	 * Returns pay method of invoice
	 *
	 * @return string
	 */
	public function getPayMethod(): string;

	/**
	 * Returns currency code of invoice (Kč, EUR,..)
	 *
	 * @return mixed
	 */
	public function getCurrency(): string;

}
