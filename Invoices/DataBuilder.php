<?php

namespace Invoices;

use DateTime;


/**
 * DataBuilder - part of Eciovni plugin for Nette Framework.
 *
 * @copyright  Copyright (c) 2009 Ondřej Brejla
 * @license    New BSD License
 * @link       http://github.com/OndrejBrejla/Eciovni
 */
class DataBuilder
{

	/** @var string */
	private $title;

	/** @var string */
	private $caption = '';

	/** @var string */
	private $signatureText = '';

	/** @var string */
	private $signatureImgSrc = '';

	/** @var string */
	private $id;

	/** @var IParticipant */
	private $supplier;

	/** @var IParticipant */
	private $customer;

	/** @var int */
	private $variableSymbol = 0;

	/** @var int */
	private $constantSymbol = 0;

	/** @var int */
	private $specificSymbol = 0;

	/** @var \DateTime */
	private $expirationDate;

	/** @var \DateTime */
	private $dateOfIssuance;

	/** @var \DateTime */
	private $dateOfVatRevenueRecognition;

	/** @var IItem[] */
	private $items = array();

	/** @var string */
	private $payMethod = '';

	/** @var string */
	private $currency = '';

	public function __construct(string $id
		, string $title
		, IParticipant $supplier
		, IParticipant $customer
		, \DateTime $expirationDate
		, \DateTime $dateOfIssuance
		, array $items
		, string $payMethod
		, $currency
	)
	{
		$this->id = $id;
		$this->title = $title;
		$this->supplier = $supplier;
		$this->customer = $customer;
		$this->expirationDate = $expirationDate;
		$this->dateOfIssuance = $dateOfIssuance;
		$this->addItems($items);
		$this->payMethod = $payMethod;
		$this->currency = $currency;
	}

	/**
	 * @return string
	 */
	public function getCaption(): string
	{
		return $this->caption;
	}

	public function setCaption(string $caption): void
	{
		$this->caption = $caption;
	}

	/**
	 * @return string
	 */
	public function getSignatureText(): string
	{
		return $this->signatureText;
	}

	public function setSignatureText(string $signatureText): void
	{
		$this->signatureText = $signatureText;
	}

	/**
	 * @return string
	 */
	public function getSignatureImgSrc(): string
	{
		return $this->signatureImgSrc;
	}

	public function setSignatureImgSrc(string $signatureImgSrc): void
	{
		$this->signatureImgSrc = $signatureImgSrc;
	}

	/**
	 * Adds array of items to the invoice.
	 *
	 * @param IItem[] $items
	 * @return void
	 */
	private function addItems($items): void
	{
		foreach($items as $item)
		{
			$this->addItem($item);
		}
	}

	/**
	 * Adds an item to the invoice.
	 *
	 * @param IItem $item
	 * @return void
	 */
	private function addItem(IItem $item): void
	{
		$this->items[] = $item;
	}

	/**
	 * Sets the variable symbol.
	 *
	 * @param int $variableSymbol
	 * @return DataBuilder
	 */
	public function setVariableSymbol($variableSymbol): self
	{
		$this->variableSymbol = $variableSymbol;

		return $this;
	}

	/**
	 * Sets the constant symbol.
	 *
	 * @param int $constantSymbol
	 * @return DataBuilder
	 */
	public function setConstantSymbol($constantSymbol): self
	{
		$this->constantSymbol = $constantSymbol;

		return $this;
	}

	/**
	 * Sets the specific symbol.
	 *
	 * @param int $specificSymbol
	 * @return DataBuilder
	 */
	public function setSpecificSymbol($specificSymbol): self
	{
		$this->specificSymbol = $specificSymbol;

		return $this;
	}

	/**
	 * Sets the date of VAT revenue recognition.
	 *
	 * @param DateTime $dateOfTaxablePayment
	 * @return DataBuilder
	 */
	public function setDateOfVatRevenueRecognition(DateTime $dateOfTaxablePayment): self
	{
		$this->dateOfVatRevenueRecognition = $dateOfTaxablePayment;

		return $this;
	}

	/**
	 * Sets the pay method for current invoice
	 *
	 * @param string $payMethod
	 * @return DataBuilder
	 */
	public function setPayMethod(string $payMethod): self
	{
		$this->payMethod = $payMethod;

		return $this;
	}

	/**
	 * Returns the invoice title.
	 *
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * Returns the invoice id.
	 *
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * Returns the invoice supplier.
	 *
	 * @return IParticipant
	 */
	public function getSupplier(): IParticipant
	{
		return $this->supplier;
	}

	/**
	 * Returns the invoice customer.
	 *
	 * @return IParticipant
	 */
	public function getCustomer(): IParticipant
	{
		return $this->customer;
	}

	/**
	 * Returns the variable symbol.
	 *
	 * @return int
	 */
	public function getVariableSymbol(): int
	{
		return $this->variableSymbol;
	}

	/**
	 * Returns the constant symbol.
	 *
	 * @return int
	 */
	public function getConstantSymbol(): int
	{
		return $this->constantSymbol;
	}

	/**
	 * Returns the specific symbol.
	 *
	 * @return int
	 */
	public function getSpecificSymbol(): int
	{
		return $this->specificSymbol;
	}

	/**
	 * Returns the expiration date in defined format.
	 *
	 * @return \DateTime
	 */
	public function getExpirationDate(): \DateTime
	{
		return $this->expirationDate;
	}

	/**
	 * Returns the date of issuance in defined format.
	 *
	 * @return \DateTime
	 */
	public function getDateOfIssuance(): \DateTime
	{
		return $this->dateOfIssuance;
	}

	/**
	 * Was VAT date revenue recognition ever set?
	 *
	 * @return bool
	 */
	public function isDateOfVatRevenueRecognition(): bool
	{
		return $this->dateOfVatRevenueRecognition instanceof \DateTime;
	}

	/**
	 * Returns the date of VAT revenue recognition in defined format.
	 *
	 * @return \DateTime
	 * @throws DataErrorException
	 */
	public function getDateOfVatRevenueRecognition(): \DateTime
	{
		if($this->dateOfVatRevenueRecognition instanceof \DateTime)
		{
			throw new DataErrorException('Value of VAT date revenue was never set');
		}

		return $this->dateOfVatRevenueRecognition;
	}

	/**
	 * Returns the array of items.
	 *
	 * @return IItem[]
	 */
	public function getItems(): array
	{
		return $this->items;
	}

	/**
	 * Gets pay method for current invoice
	 *
	 * @return string
	 */
	public function getPayMethod(): string
	{
		return $this->payMethod;
	}

	/**
	 * Returns new Data.
	 *
	 * @return IData
	 */
	public function build(): DataImpl
	{
		return new DataImpl($this);
	}

	/**
	 * Returns currency of invoice
	 *
	 * @return string
	 */
	public function getCurrency(): string
	{
		return $this->currency;
	}

}
